/**
 * Created by Neppo_antonio on 02/10/2014.
 */
'use strict';

define([
  'angular',
  'services',
  'app/modules/controllers/user.controller',
  'app/modules/controllers/gender.controller'],
  function (
    angular,
    services,
    UserController,
    GenderController) {

    /* Controllers */

    var _m = angular.module('locadora.controllers', ['locadora.services']);
    _m.controller('UserController', UserController);
    _m.controller('GenderController', GenderController);
    return _m;

  }
);