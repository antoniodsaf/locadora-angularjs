/**
 * Created by Neppo_antonio on 01/10/2014.
 */
define([], function() {
var UserService = function($resource){
    var uri = 'http://limitless-eyrie-6356.herokuapp.com/api/users';
    var Usuario = $resource(uri + '/:id', { id: '@id' }, {
        update: {
            method: 'PUT'
        },
        persist: {
            method: 'POST'
        }
    });

    return {
        getAll: function (funcaoSucesso, funcaoErro) {
            Usuario.query({}, funcaoSucesso, funcaoErro);
        },
        get: function (id, funcaoSucesso, funcaoErro) {
            Usuario.get({ id: id }, funcaoSucesso, funcaoErro);
        },
        delete: function (id, funcaoSucesso, funcaoErro) {
            Usuario.remove({id: id}, funcaoSucesso, funcaoErro);
        },
        create: function (obj, funcaoSucesso, funcaoErro) {
            obj.id = null;
            Usuario.persist({}, obj, funcaoSucesso, funcaoErro);
        },
        update: function (id, obj, funcaoSucesso, funcaoErro) {
            Usuario.update({ id: id }, obj, funcaoSucesso, funcaoErro);
        }

    };
};
    return [
        '$resource',
        UserService];
});