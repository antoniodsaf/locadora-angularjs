define([], function()
{
  var GenderService = function($resource)
  {
    var uri = 'http://limitless-eyrie-6356.herokuapp.com/api/genders';
    var Gender = $resource(uri + '/:id', { id: '@id' }, {
      update: {
        method: 'PUT'
      },
      persist:{
        method: 'POST'
      }
    });

    return {
      getAll : function(funcaoSucesso, funcaoErro){
          Gender.query({}, funcaoSucesso, funcaoErro);
      },
      get : function(id, funcaoSucesso, funcaoErro){
        Gender.get({ id: id }, funcaoSucesso, funcaoErro);
      },
      delete : function(id, funcaoSucesso, funcaoErro){
          Gender.remove({id : id}, funcaoSucesso, funcaoErro);
      },
      create : function(gender, funcaoSucesso, funcaoErro){
        gender.id = null;
        Gender.persist({}, gender, funcaoSucesso, funcaoErro);
      },
      update : function (id, obj, funcaoSucesso, funcaoErro){
        Gender.update({ id:id }, obj, funcaoSucesso, funcaoErro);
      }

    }
  };
  return [
  '$resource',
  GenderService];
});