/**
 * Created by Neppo_antonio on 01/10/2014.
 */
define([], function() {

    var UserController = function($rootScope,
                                          $scope,
                                          $filter,
                                          $alert,
                                          $userService) {
        $scope.clear = function(){
            $scope.user = {};
        }

        $scope.change = function(obj){
            $userService.update(obj['id'], obj, function(data){
                for(var i=0;i<$scope.users.length;i++){
                    if($scope.users[i]['id'] == data['id']){
                        $scope.users[i] = data;
                        break;
                    }
                }
                $scope.user = {};
                $alert.info("User alterado com sucesso!");
            }, function(error){
                $alert.error(error.statusText);
                console.log(error);
            });
        };
        $scope.remove = function(id){
            $userService.delete(id, function(data){
                for(var i=0;i<$scope.users.length;i++){
                    if($scope.users[i]['id'] == id){
                        $scope.users.splice(i, 1);
                        i--;
                    }
                }
                $alert.info("User removido com sucesso!");
            }, function(error){
                $alert.error(error.statusText);
                console.log(error);
            });
        };
        $scope.save = function(){
            var obj = angular.copy($scope.user);
            $userService.create(obj, function(data){
                if($scope.users == undefined ||
                    $scope.users == null ||
                    $scope.users.length == 0){
                    $scope.users = [];
                }
                $scope.users.push(obj);
                $scope.user = {};
                $alert.info("User salvo com sucesso!");
            }, function(error){
                $alert.error(error.statusText);
                console.log(error);
            });
        };
        $scope.searchAll = function(){
            $userService.getAll(function(data){
                $scope.users = data;
            }, function(error){
                console.log(error);
            });

        };
        $scope.prepareEdit = function(obj){
            $scope.user = angular.copy(obj);
        }


    }
    return [
        '$rootScope',
        '$scope',
        '$filter',
        '$alert',
        '$userService',
        UserController];
});