/**
 * Created by Neppo_antonio on 01/10/2014.
 */
 define([], function() {

  var GenderController = function($rootScope,
    $scope,
    $filter,
    $alert,
    $genderService) {
    $scope.gender = {};

    $scope.clear = function(){
        $scope.gender = {};
    }

    $scope.change = function(obj){
        $genderService.update(obj['id'], obj, function(data){
            for(var i=0;i<$scope.genders.length;i++){
                if($scope.genders[i]['id'] == data['id']){
                    $scope.genders[i] = data;
                    break;
                }
            }
            $scope.gender = {};
            $alert.info("Genero alterado com sucesso!");
        }, function(error){
            $alert.error(error.statusText);
            console.log(error);
        });
    };
    $scope.remove = function(id){
      $genderService.delete(id, function(data){
        for(var i=0;i<$scope.genders.length;i++){
          if($scope.genders[i]['id'] == id){
            $scope.genders.splice(i, 1);
            i--;
          }
        }
        $alert.info("Genero removido com sucesso!");
      }, function(error){
        $alert.error(error.statusText);
        console.log(error);
      });
    };
    $scope.save = function(){
      var obj = angular.copy($scope.gender);
      $genderService.create(obj, function(data){
            if($scope.genders == undefined ||
              $scope.genders == null ||
              $scope.genders.length == 0){
              $scope.genders = [];
          }
          $scope.genders.push(obj);
          $scope.gender = {};
          $alert.info("Genero salvo com sucesso!");
      }, function(error){
          $alert.error(error.statusText);
          console.log(error);
      });
    };
    $scope.searchAll = function(){
      $genderService.getAll(function(data){
        $scope.genders = data;
      }, function(error){
        console.log(error);
      });

    };
    $scope.prepareEdit = function(obj){
      $scope.gender = angular.copy(obj);
    }


  }
  return [
  '$rootScope',
  '$scope',
  '$filter',
  '$alert',
  '$genderService',
  GenderController];
});