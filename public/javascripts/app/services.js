/**
 * Created by Neppo_antonio on 02/10/2014.
 */
'use strict';
var deps = [
  'angular'
  ,'app/modules/services/user.service'
  ,'app/modules/services/gender.service'
  ,'app/modules/services/alert.ui.service'];
  define(deps, function (
    angular,
    userService,
    genderService,
    alertUI) {

    /* Services */
    // Demonstrate how to register services
    // In this case it is a simple value service.
    var _m = angular.module('locadora.services', []);
    _m.factory('$userService', userService);
    _m.factory('$genderService', genderService);
    _m.factory('$alert', alertUI);
    return _m;

  }
);
